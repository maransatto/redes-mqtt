var mqtt;
var reconnectTimeout = 2000;
var host = "test.mosquitto.org";
var port = 8080;

var sala_atual = 'fatec/chat/D1'
var salas = [];
var mensagens = []

function onConnect() {

    // Uma vez que a conexão foi estabelecida, cria umaf inscrição e envia uma mensagem

    mqtt.subscribe("fatec/chat/#") // este é meu subscriber (ou listener)
    console.log('Conectado ...');
    mensagem = new Paho.MQTT.Message("Maransatto Entrou! A Terra é Plana!! Pesquise!!");
    mensagem.destinationName = sala_atual // este de certa forma está publicando, mas não é um publisher, eu acho
    mqtt.send(mensagem);
}

function onFailure(message) {
    console.log('Conexão efetuada no host ' + host + ' falhou');
    setTimeout(MQTTconnect, reconnectTimeout); // tenta reconectar quando falha
}

function onAtualizaSalas() {

    document.getElementById("front-salas").innerHTML = '';
    
    for (const sala in salas) {
        if (salas.hasOwnProperty(sala)) {
            const element = salas[sala];

            nome_sala = element
            if (element == sala_atual) {
                nome_sala = '* ' + nome_sala
            }

            elemento_sala = `<div style="cursor:pointer" class="media text-muted pt-3">
                <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <div class="d-flex justify-content-between align-items-center w-100">
                        <strong class="text-gray-dark" onclick="onChangeSala('`+nome_sala+`'); return false;" >` + nome_sala + `</strong>
                    </div>
                </div></div>`

            document.getElementById("front-salas").insertAdjacentHTML('afterbegin', elemento_sala);
        }
    }
}

function onCreateSala(nome) {
    salas.push('fatec/chat/'+nome);
    onChangeSala('fatec/chat/'+nome);
}

function onChangeSala(sala) {
    sala_atual = sala;
    onAtualizaSalas();
    sendMessage('Maransatto Entrou! A Terra é Plana!! Pesquise!!');
}

function onMessageArrived(msg) {

    mensagens.push({
        sala: msg.destinationName,
        mensagem: msg.payloadString
    })

    document.getElementById("msg-received").innerHTML = '';

    for (const _msg in mensagens) {
        if (mensagens.hasOwnProperty(_msg)) {
            const element = mensagens[_msg];
            
            if (element.sala == sala_atual) {
                    elemento_msg = `<div class="media text-muted pt-3">
                    <img data-src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
                <div class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <div class="d-flex justify-content-between align-items-center w-100">
                    <strong class="text-gray-dark">` + element.sala + `</strong>
                    </div>
                    <span class="d-block">` + element.mensagem + `</span>
                </div></div>`

                document.getElementById("msg-received").insertAdjacentHTML('afterbegin', elemento_msg);
            }
        }
    }

    

    out_msg = ""
    out_msg = out_msg + "Sala: " + msg.destinationName + '<br>';
    out_msg = out_msg + "Mensagem recebida: " + msg.payloadString + "<br>";
    
    if (salas.indexOf(msg.destinationName) < 0) {
        salas.push(msg.destinationName) // Inclui nova sala no array
        onAtualizaSalas() // Atualiza menu de salas
    }

    console.log(out_msg);
}

function sendMessage(msg) {
    mensagem = new Paho.MQTT.Message(msg);
    mensagem.destinationName = sala_atual // este de certa forma está publicando, mas não é um publisher, eu acho
    mqtt.send(mensagem);
}

function MQTTconnect() {
    console.log('Conectando no servidor ' + host + ':' + port);
    mqtt = new Paho.MQTT.Client(host, port, "Maransatto");

    var options = {
        timeout: 3,
        onSuccess: onConnect,
        onFailure: onFailure
    };

    mqtt.onMessageArrived = onMessageArrived;
    mqtt.connect(options)
}